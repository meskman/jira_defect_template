// Priority icons logic here
chrome.tabs.onCreated.addListener(do_css_injection);
chrome.tabs.onUpdated.addListener(function(tabId, info, tab) {
    if (info.status == 'complete') do_css_injection(tab);
});

function inject(tab){
    // perform css inject function
    var tabUrl = tab.url;
    //console.log(tabUrl);
    //console.log(tabUrl.indexOf("jira.bbqnx.net"));
    if (tabUrl && tabUrl.indexOf("jira.bbqnx.net") != -1) {
        chrome.tabs.insertCSS(tab.id, {
            file: "css/inject.css"
        });
    }
}
function do_css_injection(tab) {
    // get if checkbox is checked in options
    chrome.storage.sync.get('priorityIcons', function(response) {
    //console.log(response.priorityIcons);
    if (response.priorityIcons == true) inject(tab);
    if (typeof response.priorityIcons == 'undefined') inject(tab);
    });

}
// The onClicked callback function.
function onClickHandler(info, tab) {
  //console.log("item " + info.menuItemId + " was clicked");
  //console.log("info: " + JSON.stringify(info));
  //console.log("tab: " + JSON.stringify(tab));

  // get options info in context menu item click
  chrome.storage.sync.get('textarea', function(response) {
  if (typeof response.textarea == 'undefined') response.textarea = "Attention! You do not have any template configured..";
  //console.log(response.textarea);
  // Basically it send a message to content page to run the according function there
  // function is corresponding to ID. info.menuItemId will be MenuItem1 for the first menu item and text is template text 
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
  chrome.tabs.sendMessage(tabs[0].id, {"action": info.menuItemId, "text": response.textarea});
     });
  });        
    
};

chrome.contextMenus.onClicked.addListener(onClickHandler);

// Set up context menu tree at install time.
chrome.runtime.onInstalled.addListener(function() {
  // Create menu items here  
    var menuitem1_title = "Insert JIRA Defect Template";
    var id = chrome.contextMenus.create({"title": menuitem1_title, "contexts":["editable"],
                                         "id": "MenuItem1"});
    
});
