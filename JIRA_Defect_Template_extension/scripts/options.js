// Saves options to chrome.storage.sync.
function save_options() {
 
  var textarea = document.getElementById('ttext').value;
  var priorityIcons = document.getElementById('priority_icons').checked;
  chrome.storage.sync.set({
  	textarea: textarea,
  	priorityIcons: priorityIcons

  }, function() {
    // Update status to let user know options were saved.
    var status = document.getElementById('status');
    status.textContent = 'Options saved.';
    setTimeout(function() {
      status.textContent = '';
    }, 750);
  });
}

// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
function restore_options() {  
  chrome.storage.sync.get({
  	textarea: 'default text',
    priorityIcons: true
  }, function(items) {
 
    document.getElementById('ttext').value = items.textarea;
    document.getElementById('priority_icons').checked = items.priorityIcons;
  });
}
document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click',
    save_options);