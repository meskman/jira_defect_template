# README #

JIRA Defect Template

### What is this repository for? ###

* This extension allows to add predefined template to JIRA defect's description
* Version 1.3

### How do I get set up? ###

1. Download .crx file
2. Drag & drop it to Chrome browser
3. Go to chrome://extension and click on option link next to installed extension
4. Insert template for defects and save it
5. Go to Jira and create a new defect
6. Move to 'Description' field and call context menu
7. In context menu click on 'Inset JIRA Defect Template' item
8. Observe that template has appeared in 'Description' field

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact